function Game() {
    //监听键盘点击事件
    this.addEvent();
}

Game.prototype.init = function () {
    this.score = 0;
    $("#score_input").html("分数是：0");
    this.arr = [];
    $(".number_cell").remove();
    this.moveAble = false;
    this.creatArr();
}

Game.prototype.creatArr = function () {
    //生成原始数组
    var i, j;
    for (let i = 0; i < 4; i++) {
        this.arr[i] = [];
        for (let j = 0; j < 4; j++) {
            this.arr[i][j] = {};
            this.arr[i][j].value = 0;
        }
    }
    //随机生成两个格子坐标 dowhile来实现不重复随机 坐标[0.3]
    var i1, j1, i2, j2;
    do {
        i1 = Math.floor(Math.random() * 4);
        j1 = Math.floor(Math.random() * 4);
        i2 = Math.floor(Math.random() * 4);
        j2 = Math.floor(Math.random() * 4);
    } while (i1 == i2 && j1 == j2)

    this.updateCellValue(2, i1, j1);
    this.updateCellValue(2, i2, j2);
    this.drawOneCell(i1, j1);
    this.drawOneCell(i2, j2);
}

Game.prototype.updateCellValue = function (num, i, j) {
    this.arr[i][j].value = num;
}

Game.prototype.drawOneCell = function (i, j) {
    //在i行j列画一个新格子
    var item = '<div class="number_cell p' + i + j + '" ><div class="number_cell_con n2"><span>'
        + this.arr[i][j].value + '</span></div> </div>';
    $(".g2048").append(item);
}

Game.prototype.addEvent = function () {
    let that = this;
    document.onkeydown = function (event) {
        var e = event || window.event || arguments.callee.caller.arguments[0];
        switch (e.keyCode) {
            case 38: //上
                that.moveAble = false;
                that.moveUp();
                that.checkLose();
                break;
            case 40: //下
                that.moveAble = false;
                that.moveDown();
                that.checkLose();
                break;
            case 37: //左
                that.moveAble = false;
                that.moveLeft();
                that.checkLose();
                break;
            case 39: //右
                that.moveAble = false;
                that.moveRight();
                that.checkLose();
                break;
        }
    }
}

// 00 10 20 30
// 01 11 21 31
// 02 12 22 32
// 03 13 23 33

Game.prototype.moveUp = function () {
    var i, j, k, n;
    for (i = 0; i < 4; i++) {
        n = 0;
        for (j = 0; j < 4; j++) {
            if (this.arr[i][j].value == 0) {
                //如果这个格子没有 则进入下一个循环
                continue;
            }
            //如果这个格子上有东西
            k = j - 1;
            while (k >= n) {
                if (this.arr[i][k].value == 0) {
                    //上面的格子是空的 则移动到k格子
                    if (k == n || (this.arr[i][k - 1].value != 0 && this.arr[i][k - 1].value != this.arr[i][j].value)) {
                        this.moveCell(i, j, i, k);
                    }
                    k--;
                } else {
                    if (this.arr[i][k].value == this.arr[i][j].value) {
                        this.mergeCells(i, j, i, k);
                        n++;
                    }
                    break;
                }
            }
        }
    }
}

Game.prototype.moveDown = function () {
    /*向下移动*/
    var i, j, k, n;
    for (i = 0; i < 4; i++) {
        n = 3;
        for (j = 3; j >= 0; j--) {
            if (this.arr[i][j].value == 0) {
                continue;
            }
            k = j + 1;
            aa: while (k <= n) {
                if (this.arr[i][k].value == 0) {
                    if (k == n || (this.arr[i][k + 1].value != 0 && this.arr[i][k + 1].value != this.arr[i][j].value)) {
                        this.moveCell(i, j, i, k);
                    }
                    k++;
                }
                else {
                    if (this.arr[i][k].value == this.arr[i][j].value) {
                        this.mergeCells(i, j, i, k);
                        n--;
                    }
                    break aa;
                }
            }
        }
    }
    this.newCell(); //生成一个新格子。后面要对其做判断。
};
Game.prototype.moveLeft = function () {
    /*向左移动*/
    var i, j, k, n;
    for (j = 0; j < 4; j++) {
        n = 0;
        for (i = 0; i < 4; i++) {
            if (this.arr[i][j].value == 0) {
                continue;
            }
            k = i - 1;
            aa: while (k >= n) {
                if (this.arr[k][j].value == 0) {
                    if (k == n || (this.arr[k - 1][j].value != 0 && this.arr[k - 1][j].value != this.arr[i][j].value)) {
                        this.moveCell(i, j, k, j);
                    }
                    k--;
                }
                else {
                    if (this.arr[k][j].value == this.arr[i][j].value) {
                        this.mergeCells(i, j, k, j);
                        n++;
                    }
                    break aa;
                }
            }
        }
    }
    this.newCell(); //生成一个新格子。后面要对其做判断。
};
Game.prototype.moveRight = function () {
    /*向右移动*/
    var i, j, k, n;
    for (j = 0; j < 4; j++) {
        n = 3;
        for (i = 3; i >= 0; i--) {
            if (this.arr[i][j].value == 0) {
                continue;
            }
            k = i + 1;
            aa: while (k <= n) {
                if (this.arr[k][j].value == 0) {
                    if (k == n || (this.arr[k + 1][j].value != 0 && this.arr[k + 1][j].value != this.arr[i][j].value)) {
                        this.moveCell(i, j, k, j);
                    }
                    k++;
                }
                else {
                    if (this.arr[k][j].value == this.arr[i][j].value) {
                        this.mergeCells(i, j, k, j);
                        n--;
                    }
                    break aa;
                }
            }
        }
    }
    this.newCell(); //生成一个新格子。后面要对其做判断。
};

Game.prototype.newCell = function () {
    /*在空白处掉下来一个新的格子*/
    var i, j, len, index;
    var ableArr = [];
    if (this.moveAble != true) {
        console.log('不能增加新格子，请尝试其他方向移动！');
        return;
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            if (this.arr[i][j].value == 0) {
                ableArr.push([i, j]);
            }
        }
    }
    len = ableArr.length;
    if (len > 0) {
        index = Math.floor(Math.random() * len);
        i = ableArr[index][0];
        j = ableArr[index][1];
        this.updateCellValue(2, i, j);
        this.drawOneCell(i, j);
    }
    else {
        console.log('没有空闲的格子了！');
        return;
    }
};

Game.prototype.moveCell = function (i1, j1, i2, j2) {
    this.arr[i2][j2].value = this.arr[i1][j1].value;
    this.arr[i1][j1].value = 0;
    this.moveAble = true;
    $(".p" + i1 + j1).removeClass("p" + i1 + j1)
        .addClass("p" + i2 + j2);
}

Game.prototype.mergeCells = function (i1, j1, i2, j2) {
    var temp = this.arr[i2][j2].value;
    var temp1 = temp * 2;
    this.moveAble = true;
    this.arr[i2][j2].value = temp1;
    this.arr[i1][j1].value = 0;
    $(".p" + i2 + j2).addClass('toRemove');
    var theDom = $(".p" + i1 + j1).removeClass("p" + i1 + j1).addClass("p" + i2 + j2).find('.number_cell_con');
    setTimeout(function () {
        $(".toRemove").remove();
        theDom.addClass('n' + temp1).removeClass('n' + temp).find('span').html('' + temp1);
    }, 200);

    this.updateScore(temp1);
}

Game.prototype.updateScore = function (temp1) {
    this.score += temp1;
    $("#score_input").html("分数：" + this.score);
    if (this.score == 2048) {
        alert("you win!")
        this.init();
    }
}

Game.prototype.checkLose = function () {
    /*判输*/
    var i, j, temp;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            temp = this.arr[i][j].value;
            if (temp == 0) {
                return false;
            }
            if (this.arr[i + 1] && (this.arr[i + 1][j].value == temp)) {
                return false;
            }
            if ((this.arr[i][j + 1] != undefined) && (this.arr[i][j + 1].value == temp)) {
                return false;
            }
        }
    }
    alert('you lose!');
    this.init();
    return true;
};

let game = new Game();
game.init();